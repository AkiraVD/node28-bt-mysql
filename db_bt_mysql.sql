#tạo database:
CREATE DATABASE db_bt_mysql;

#tạo các table:
CREATE TABLE user (
    user_id INT PRIMARY KEY AUTO_INCREMENT,
    full_name VARCHAR(100),
    email VARCHAR(100),
    password VARCHAR(100)
);

CREATE TABLE food_type (
    type_id INT PRIMARY KEY AUTO_INCREMENT,
    type_name VARCHAR(100)
);

CREATE TABLE food (
    food_id INT PRIMARY KEY AUTO_INCREMENT,
    food_name VARCHAR(100),
    food_image VARCHAR(100),
    food_price FLOAT,
    food_desc VARCHAR(100),
    type_id INT,
    FOREIGN KEY (type_id) REFERENCES food_type(type_id)
);

CREATE TABLE sub_food (
    sub_id INT PRIMARY KEY AUTO_INCREMENT,
    sub_name VARCHAR(100),
    sub_price FLOAT,
    food_id INT,
    FOREIGN KEY (food_id) REFERENCES food(food_id)
);

CREATE TABLE restaurant (
    res_id INT PRIMARY KEY AUTO_INCREMENT,
    res_name VARCHAR(100),
    res_image VARCHAR(100),
    res_desc VARCHAR(100)
);

CREATE TABLE rate_res (
    user_id INT,
    res_id INT,
    amount INT,
    date_rate DATETIME,
    FOREIGN KEY (user_id) REFERENCES user(user_id),
    FOREIGN KEY (res_id) REFERENCES restaurant(res_id)
);

CREATE TABLE like_res (
    user_id INT,
    res_id INT,
    date_like DATETIME,
    FOREIGN KEY (user_id) REFERENCES user(user_id),
    FOREIGN KEY (res_id) REFERENCES restaurant(res_id)
);

CREATE TABLE food_order (
    user_id INT,
    food_id INT,
    amount INT,
    code VARCHAR(100),
    arr_sub_id VARCHAR(100),
    FOREIGN KEY (user_id) REFERENCES user(user_id),
    FOREIGN KEY (food_id) REFERENCES food(food_id)
);

# thêm data
INSERT INTO user (full_name, email, password)
    VALUES ("John Doe", "johndoe@example.com", "password1"),
    ("Jane Smith", "janesmith@example.com", "password2"),
    ("Bob Johnson", "bobjohnson@example.com", "password3"),
    ("Emily Davis", "emilydavis@example.com", "password4"),
    ("Michael Brown", "michaelbrown@example.com", "password5"),
    ("Sarah Miller", "sarahmiller@example.com", "password6"),
    ("David Wilson", "davidwilson@example.com", "password7"),
    ("Jessica Moore", "jessicamoore@example.com", "password8"),
    ("William Taylor", "williamtaylor@example.com", "password9"),
    ("Ashley Anderson", "ashleyanderson@example.com", "password10")
;

INSERT INTO food_type (type_name) VALUES ("Italian"), ("Mexican"), ("Chinese"), ("Japanese");

INSERT INTO food (food_name, food_image, food_price, food_desc, type_id) VALUES
    ("Spaghetti", "spaghetti.jpg", 12.99, "Spaghetti with marinara sauce", 1),
    ("Tacos", "tacos.jpg", 8.99, "Soft shell beef tacos", 2),
    ("Fried Rice", "fried_rice.jpg", 9.99, "Shrimp fried rice", 3),
    ("Sushi", "sushi.jpg", 15.99, "Assorted sushi platter", 4),
    ("Lasagna", "lasagna.jpg", 13.99, "Classic meat lasagna", 1),
    ("Enchiladas", "enchiladas.jpg", 9.99, "Chicken enchiladas", 2),
    ("Lo Mein", "lo_mein.jpg", 10.99, "Vegetable lo mein", 3),
    ("Ramen", "ramen.jpg", 12.99, "Shoyu chicken ramen", 4),
    ("Pizza", "pizza.jpg", 14.99, "Pepperoni pizza", 1),
    ("Burrito", "burrito.jpg", 10.99, "Beef and bean burrito", 2)
;

INSERT INTO sub_food (sub_name, sub_price, food_id) VALUES
    ("Extra Meat", 2.99, 1),
    ("Extra cheese", 1.99, 2),
    ("Extra veggies", 1.49, 3),
    ("Extra sauce", 0.99, 4),
    ("Extra egg", 0.99, 5)
;

INSERT INTO restaurant (res_name, res_image, res_desc) VALUES
    ("Papa John's Pizza", "papa_johns.jpg", "Family friendly pizza restaurant"),
    ("Taco Bell", "taco_bell.jpg", "Fast food Mexican restaurant"),
    ("Sakura Sushi", "sakura_sushi.jpg", "Fine dining Japanese restaurant")
;

INSERT INTO food_order (user_id, food_id, amount, code, arr_sub_id) VALUES 
    (1, 1, 2, "1234", "1,2"), 
    (2, 2, 3, "2345", "3"), 
    (3, 3, 1, "3456", "4"), 
    (4, 4, 2, "4567", "1"), 
    (4, 1, 1, "5678", "2"), 
    (5, 2, 2, "6789", "3"), 
    (6, 3, 3, "7890", "4"), 
    (1, 4, 1, "8901", "1,2,3"),
    (2, 2, 1, "1234", "3"), 
    (3, 3, 2, "2345", "4"), 
    (4, 1, 3, "3456", "1,2"), 
    (5, 4, 1, "4567", "2"), 
    (1, 2, 2, "5678", "3")
;

INSERT INTO rate_res (user_id, res_id, amount, date_rate) VALUES 
    (1, 1, 4, "2022-01-01 12:00:00"), 
    (2, 2, 3, "2022-02-01 12:00:00"), 
    (3, 3, 5, "2022-03-01 12:00:00"), 
    (4, 1, 4, "2022-04-01 12:00:00"), 
    (5, 2, 2, "2022-05-01 12:00:00"),
    (1, 2, 4, "2022-01-01 12:00:00"), 
    (2, 3, 3, "2022-02-01 12:00:00"), 
    (3, 1, 5, "2022-03-01 12:00:00"), 
    (4, 2, 4, "2022-04-01 12:00:00"), 
    (5, 3, 2, "2022-05-01 12:00:00")
;

INSERT INTO like_res (user_id, res_id, date_like) VALUES 
    (1, 1, "2022-01-01 12:00:00"), 
    (2, 2, "2022-02-01 12:00:00"), 
    (3, 3, "2022-03-01 12:00:00"), 
    (1, 2, "2022-04-01 12:00:00"), 
    (2, 3, "2022-05-01 12:00:00"), 
    (5, 3, "2022-06-01 12:00:00"), 
    (2, 1, "2022-07-01 12:00:00")
;

# câu trả lời bài tập:

# tìm 5 người like nhà hàng nhiều nhất
SELECT user_id, COUNT(*) as like_count
    FROM like_res
    GROUP BY user_id
    ORDER BY like_count DESC
    LIMIT 5
;

# tìm 2 nhà hàng có lượt like nhiều nhất
SELECT res_id, COUNT(*) as like_count
    FROM like_res
    GROUP BY res_id
    ORDER BY like_count DESC
    LIMIT 2
;

# tìm khách hàng đã order nhiều nhất
SELECT user_id, COUNT(*) as order_count
FROM food_order
GROUP BY user_id
ORDER BY order_count DESC
LIMIT 1;

# tìm người dùng không hoạt động
SELECT user.user_id, user.full_name
FROM user
WHERE user.user_id NOT IN (
    SELECT user_id FROM food_order
    UNION
    SELECT user_id FROM rate_res
    UNION
    SELECT user_id FROM like_res
);

# trung bình giá sub_food của 1 food:
SELECT food.food_name, AVG(sub_food.sub_price) as avg_sub_price
    FROM food
    LEFT JOIN sub_food ON food.food_id = sub_food.food_id
    GROUP BY food.food_name
;